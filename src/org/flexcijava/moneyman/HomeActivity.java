package org.flexcijava.moneyman;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

public class HomeActivity extends FragmentActivity {

	int NUMBER_OF_ACCOUNTS = 1;
	
    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new SectionFragment();
            return fragment;
        }

        @Override
        public int getCount() {
            return NUMBER_OF_ACCOUNTS; //TODO
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
            //TODO
                case 0: return "All Accounts";
                case 1: return "Account #";
                case 2: return "Account #";
            }
            return null;
        }
    } // end of class FragmentPagerAdapter

    /**
     * A dummy fragment representing a section of the app, but that simply displays dummy text.
     */
    public static class SectionFragment extends Fragment {
        public SectionFragment() {
        }
        
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
        	
        	LinearLayout linearlayout = new LinearLayout(getActivity());
        	linearlayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        	linearlayout.setPadding(10, 10, 10, 10);
        	linearlayout.setOrientation(LinearLayout.VERTICAL);
        	
            TextView textView = new TextView(getActivity());
            textView.setGravity(Gravity.CENTER);
            textView.setText("TRY");
            
            linearlayout.addView(textView);
            return linearlayout;
        }
    }
}
 